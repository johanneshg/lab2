//
//  ExperienceViewController.swift
//  lab2
//
//  Created by Johannes Hou Gustafsson on 2018-12-07.
//  Copyright © 2018 Johannes Hou Gustafsson. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var experiences: [[Experience]] = [[]]
    var titleString = ["Work Experience", "Education"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        experiences = [[Experience(imageName: "work1", expName: "Arla", expYear:"2015-2017", expDesc:"I worked Arla"),
                        Experience(imageName: "work2", expName: "Dafgårds", expYear: "2013-2014", expDesc:"I worked at Dafgårds")],
                       [Experience(imageName: "work3", expName: "Jönköping University", expYear: "2017-Current", expDesc: "I study at Jönköping University"),
                        Experience(imageName: "work4", expName: "De La Gardie", expYear: "2014-2017", expDesc:"I studied at De La Gardie")]]
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ExperienceDetailViewController {
            if let indexPath = sender as? IndexPath {
                let experience = experiences[indexPath.section][indexPath.row]
                destination.experience = experience
            }
        }
    }
    
    
}
extension ExperienceViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return experiences.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            if let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceTableViewCell", for: indexPath) as?
                ExperienceTableViewCell {
                let experience = experiences[indexPath.section][indexPath.row]
                cell.expName.text = experience.expName
                cell.expImage.image = UIImage(named: experience.imageName)
                cell.expYear.text = experience.expYear
                return cell
            } else {
                return UITableViewCell()
            }
    }
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
            headerView.backgroundColor = UIColor.darkGray
            let headerLabel = UILabel(frame: CGRect(x: 8, y: 0, width: headerView.frame.width - 16, height: 30))
            headerLabel.textColor = UIColor.white
            headerLabel.font = UIFont.boldSystemFont(ofSize: 15)
            headerLabel.text = titleString[section]
            headerLabel.textAlignment = .center
            headerView.addSubview(headerLabel)
            return headerView
            
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detailExpSegue", sender: indexPath)
    }
        
        
    
    
}
