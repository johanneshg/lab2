//
//  SkillsViewController.swift
//  lab2
//
//  Created by Johannes Hou Gustafsson on 2018-12-07.
//  Copyright © 2018 Johannes Hou Gustafsson. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {
    
    @IBOutlet weak var buttonAnimate: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonAnimate.backgroundColor = UIColor.red
        self.buttonAnimate.layer.cornerRadius = self.buttonAnimate.frame.height / 2;


        
    }
    @IBAction func buttonPressed(_ sender: UIButton) {
        sender.backgroundColor = UIColor.random
        sender.alpha = 0
        UIView.animate(withDuration: 0.3, animations: {
            sender.alpha = 1
        }, completion: { completed in
        })
    }
}
extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
