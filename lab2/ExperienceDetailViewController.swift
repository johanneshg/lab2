//
//  ExperienceDetailViewController.swift
//  lab2
//
//  Created by Johannes Hou Gustafsson on 2018-12-07.
//  Copyright © 2018 Johannes Hou Gustafsson. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {
    
    @IBOutlet weak var expWorkName: UILabel!
    @IBOutlet weak var expWorkYear: UILabel!
    @IBOutlet weak var expImage: UIImageView!
    @IBOutlet weak var expDesc: UILabel!
    var experience: Experience = Experience(imageName: "", expName: "", expYear: "", expDesc: "")


    override func viewDidLoad() {
        super.viewDidLoad()
        expImage.image = UIImage(named: experience.imageName)
        expWorkName.text = experience.expName
        expWorkYear.text = experience.expYear
        expDesc.text = experience.expDesc
        self.navigationItem.title = experience.expName

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
