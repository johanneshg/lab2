//
//  Experience.swift
//  lab2
//
//  Created by Johannes Hou Gustafsson on 2018-12-07.
//  Copyright © 2018 Johannes Hou Gustafsson. All rights reserved.
//

import Foundation

struct Experience {
    var imageName: String
    var expName: String
    var expYear: String
    var expDesc: String
}
