//
//  ViewController.swift
//  lab2
//
//  Created by Johannes Hou Gustafsson on 2018-12-07.
//  Copyright © 2018 Johannes Hou Gustafsson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var ProfilePic: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        ProfilePic.image = UIImage(named: "Palpatine01")
    }


}

