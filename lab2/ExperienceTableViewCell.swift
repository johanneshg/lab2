//
//  ExperienceTableViewCell.swift
//  lab2
//
//  Created by Johannes Hou Gustafsson on 2018-12-07.
//  Copyright © 2018 Johannes Hou Gustafsson. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {
    @IBOutlet weak var expImage: UIImageView!
    @IBOutlet weak var expName: UILabel!
    @IBOutlet weak var expYear: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
